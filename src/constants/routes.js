export const SIGN_UP = "/signup";
export const SIGN_IN = "/signin";
export const SIGN_OUT = "/signout";

export const HOME = "/";
export const ACCOUNT = "/account";
export const ADMIN = "/admin";
export const MANAGE_USERS = "/manageUsers";
export const MANAGE_EVENTS = "/manageEvents";
export const CREATE_EVENT = "/manageEvents/create";
export const VIEW_EVENT = "/manageEvents/view/:id";
export const EDIT_EVENT = `/manageEvents/edit/:id`;

export const EVENTS = "/events";
export const POINTS = "/points";

export const PASSWORD_FORGET = "/forgotPassword";
